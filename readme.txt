# clean
./02_clean.sh

# build
make

# install
mkdir include lib 
includedir=include libdir=lib make install

tree
.
├── clean.sh
├── foo.c
├── foo.h
├── foo.o
├── include
│   └── foo.h
├── lib
│   ├── libfoo.so -> libfoo.so.1.2.3
│   ├── libfoo.so.1 -> libfoo.so.1.2.3
│   └── libfoo.so.1.2.3
├── libfoo.so.1.2.3
├── Makefile
└── readme.txt
